#!/bin/bash
#
# Author: DeepPiXEL Inc. 2018
#
# Unsets environment variables based on variables passed at the command line.
# Variables passed in will be passed into numbered variables, first would be in
# variable $1, second $2, and so on.
#
# File '/etc/profile' is modified so future logins will have environment variables unset.
#
# usage: . set_env.sh <NAME1> <NAME2> ... <NAMEn>
#

echo "START unsetting environment variables for this shell..."
echo "---------------------------------"
COUNTER=1
echo "1. Unsetting variables:"
while true
do

    # if no variable name specified
    if [[ -z ${!COUNTER} ]]
    then
        # break out of loop
        break
    else
        # otherwise set name/value pair

        # get name
        NAME=${!COUNTER}
        COUNTER=$(awk "BEGIN {print $COUNTER+1; exit}")

        # unset name/value pair in current terminal
        echo "unsetting environment variable: $NAME"
        unset $NAME

        # unset name pain in /etc/profile:
        # https://social.msdn.microsoft.com/Forums/azure/en-US/995ae57f-5709-4890-97a6-7428f129e034/how-can-i-set-environment-variable-using-custom-linux-extension?forum=WAVirtualMachinesforWindows
        sed -i /${NAME}=/d /etc/environment
    fi
done

echo "---------------------------------"
echo "2. Checking if variables unset (no set variables should appear below):"

COUNTER=1
while true
do

    # if no variable name specified
    if [[ -z ${!COUNTER} ]]
    then
        # break out of loop
        break
    else
        # otherwise check unset name/value pair

        # get name
        NAME=${!COUNTER}
        COUNTER=$(awk "BEGIN {print $COUNTER+1; exit}")

        # get value
        VALUE=${!COUNTER}
        COUNTER=$(awk "BEGIN {print $COUNTER+1; exit}")

        # set name/value pair
        printenv | grep  $NAME=$VALUE
    fi
done
echo "---------------------------------"
echo DONE!

# need this script to be run like so ". set_env.sh <NAME1> <VALUE1> ..."
# which executes in current shell, so that environment variables are set in current
# shell. Thus can't have script exit or it will exit the curent shell which is not
# what we want.
# https://stackoverflow.com/questions/496702/can-a-shell-script-set-environment-variables-of-the-calling-shell
#exit 0