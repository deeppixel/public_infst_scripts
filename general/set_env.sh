#!/bin/bash
#
# Author: DeepPiXEL Inc. 2018
#
# Sets environment variables based on pairs of variables passed at the command line.
# Variables passed in will be passed into numbered variables, first would be in
# variable $1, second $2, and so on. So then names would be in odd numbered variables and
# events would be in even numbered variables.
#
# File '/etc/profile' is modified so future logins will have environment variables set. Since file is being
# modified it is important that this is only run once at deployment of vms.
#
# usage: . set_env.sh <NAME1> <VALUE1> <NAME2> <VALUE2> ... <NAMEn> <VALUEn>
#

echo "START setting environment variables for this shell..."
echo "---------------------------------"
COUNTER=1
echo "1. Setting variables:"
while true
do

    # if no variable name specified
    if [[ -z ${!COUNTER} ]]
    then
        # break out of loop
        break
    else
        # otherwise set name/value pair

        # get name
        NAME=${!COUNTER}
        COUNTER=$(awk "BEGIN {print $COUNTER+1; exit}")

        # get value
        VALUE=${!COUNTER}
        COUNTER=$(awk "BEGIN {print $COUNTER+1; exit}")

        # set name/value pair in current terminal
        echo "setting environment variable: $NAME=$VALUE"
        export $NAME=$VALUE

        # set name/value pain in /etc/environment (which is not a command line script but just contains name value pairs):
        # https://help.ubuntu.com/community/EnvironmentVariables#A.2Fetc.2Fenvironment
        # and not in /etc/profile
        # https://social.msdn.microsoft.com/Forums/azure/en-US/995ae57f-5709-4890-97a6-7428f129e034/how-can-i-set-environment-variable-using-custom-linux-extension?forum=WAVirtualMachinesforWindows
        echo "$NAME=$VALUE" >> /etc/environment
    fi
done

echo "---------------------------------"
echo "2. Checking if variables set (set variables should appear below):"

COUNTER=1
while true
do

    # if no variable name specified
    if [[ -z ${!COUNTER} ]]
    then
        # break out of loop
        break
    else
        # otherwise set name/value pair

        # get name
        NAME=${!COUNTER}
        COUNTER=$(awk "BEGIN {print $COUNTER+1; exit}")

        # get value
        VALUE=${!COUNTER}
        COUNTER=$(awk "BEGIN {print $COUNTER+1; exit}")

        # set name/value pair
        printenv | grep  $NAME=$VALUE
    fi
done
echo "---------------------------------"
echo DONE!

# need this script to be run like so ". set_env.sh <NAME1> <VALUE1> ..."
# which executes in current shell, so that environment variables are set in current
# shell. Thus can't have script exit or it will exit the curent shell which is not
# what we want.
# https://stackoverflow.com/questions/496702/can-a-shell-script-set-environment-variables-of-the-calling-shell
#exit 0

# restart machine to allow environment variables to take effect
shutdown -r -f -t 1